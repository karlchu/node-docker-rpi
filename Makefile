IMAGE=karlchu/node-rpi

build: build-4.3.1

build-0.10.28: node-v${VERSION}-linux-arm-pi.tar.gz
	VERSION=0.10.28
	docker build -f Dockerfile.0.10.28 -t ${IMAGE}:latest .
	docker tag -f ${IMAGE}:latest karlchu/node-rpi:${VERSION}

build-0.10.29:
	VERSION=0.10.29
	docker build -f Dockerfile.0.10.29 -t ${IMAGE}:latest .
	docker tag -f ${IMAGE}:latest karlchu/node-rpi:${VERSION}

build-4.3.1: node-v4.3.1-linux-armv6l.tar.gz
	VERSION=4.3.1
	docker build -f Dockerfile.4.3.1 -t ${IMAGE}:4.3.1 .

node-v${VERSION}-linux-arm-pi.tar.gz:
	curl -OL http://nodejs.org/dist/v${VERSION}/node-v${VERSION}-linux-arm-pi.tar.gz

node-v4.3.1-linux-armv6l.tar.gz:
	curl -OL https://nodejs.org/dist/v4.3.1/node-v4.3.1-linux-armv6l.tar.gz

push: build
	docker push ${IMAGE}:latest
	docker push ${IMAGE}:${VERSION}
